package io.interview.revo.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by afeozzz on 14/02/17.
 */

public class Constants {
    public static final String EURO_ID = "EURO";
    public static final String DOLLAR_ID = "USD";
    public static final String GBR_ID = "GBP";
    public static final long JOB_SCHEDULER_INTERVAL = TimeUnit.SECONDS.toMillis(3);
}
