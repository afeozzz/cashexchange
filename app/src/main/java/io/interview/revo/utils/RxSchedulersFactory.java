package io.interview.revo.utils;

import rx.Observable;
import rx.Scheduler;

public abstract class RxSchedulersFactory {

    abstract public Scheduler getMainThreadScheduler();

    abstract public Scheduler getIOScheduler();

    abstract public Scheduler getComputationScheduler();

    public <T> Observable.Transformer<T, T> applySchedulers() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> tObservable) {
                return tObservable.subscribeOn(getIOScheduler())
                        .observeOn(getMainThreadScheduler());
            }
        };
    }

}
