package io.interview.revo.utils;

import android.util.Log;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.inject.Singleton;

/**
 * Created by afeozzz on 13/02/17.
 */

@Singleton
public class ValidateCurrencies {
    private static HashSet<String> validateValues = new HashSet<>(Arrays.asList("USD", "GBP"));

    public static boolean checkCurrencies(String currency) {
        Log.d("currency", currency);
        return validateValues.contains(currency);
    }
}
