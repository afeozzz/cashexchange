package io.interview.revo.domain;

import android.util.Log;

import com.squareup.sqlbrite.SqlBrite;

import java.util.List;

import io.interview.revo.data.ExchangeRepository;
import io.interview.revo.data.local.CurrencyModel;
import io.interview.revo.data.remote.RevoApi;
import io.interview.revo.data.remote.XMLConverterFactory;
import io.interview.revo.data.remote.entities.CurrencyResponse;
import io.interview.revo.utils.RxSchedulersFactory;
import io.interview.revo.utils.ValidateCurrencies;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;

import static io.interview.revo.data.remote.entities.CurrencyResponse.*;

/**
 * Created by afeozzz on 13/02/17.
 */

public class ExchangeInteractor {

    private final ExchangeRepository repository;
    private final RxSchedulersFactory rxSchedulersFactory;

    public ExchangeInteractor(ExchangeRepository exchangeRepository, RxSchedulersFactory rxSchedulersFactory) {
        this.repository = exchangeRepository;
        this.rxSchedulersFactory = rxSchedulersFactory;
    }

    public Observable<List<CurrencyModel>> getNewCurrencies() {
        return repository.getNewCurrencies().flatMap(new Func1<List<CurrencyModel>, Observable<List<CurrencyModel>>>() {
            @Override
            public Observable<List<CurrencyModel>> call(List<CurrencyModel> currencyModels) {
                return Observable.from(currencyModels)
                        .filter(new Func1<CurrencyModel, Boolean>() {
                            @Override
                            public Boolean call(CurrencyModel currencyModel) {
                                return ValidateCurrencies.checkCurrencies(currencyModel.getId());
                            }
                        })
                        .doOnNext(new Action1<CurrencyModel>() {
                            @Override
                            public void call(CurrencyModel currencyModel) {
                                Log.d("saved currency", currencyModel.getTitle());
                                repository.saveCurrency(currencyModel);
                            }
                        }).toList();
            }
        }).compose(rxSchedulersFactory.<List<CurrencyModel>>applySchedulers());
    }


    public Observable<List<CurrencyModel>> handleCurrencyChangesObserver() {
        return repository.getCachedCurrencies();
    }

    public void saveCurrency(CurrencyModel currencyModel) {
        repository.saveCurrency(currencyModel);
    }

}
