package io.interview.revo.di.exchange;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by afeozzz on 13/02/17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ExchangeScope {
}
