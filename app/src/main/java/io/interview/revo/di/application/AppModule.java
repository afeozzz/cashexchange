package io.interview.revo.di.application;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Module;
import dagger.Provides;
import io.interview.revo.data.local.ExchangeDbHelper;
import io.interview.revo.data.local.ExchangeLocalDataSource;
import io.interview.revo.data.service.ExchangeJobScheduler;
import io.interview.revo.di.exchange.ExchangeScope;
import io.interview.revo.utils.Constants;
import io.interview.revo.utils.RxSchedulersFactory;
import io.interview.revo.utils.ValidateCurrencies;

/**
 * Created by afeozzz on 13/02/17.
 */

@Module
public class AppModule {
    private final Context appContext;

    public AppModule(@NonNull Context context) {
        appContext = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return appContext;
    }

    @Provides
    @Singleton
    JobScheduler provideJobScheduler(Context context) {
        return (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

    @Provides
    @Singleton
    JobInfo provideShedulingJob(Context context) {
        ComponentName componentName = new ComponentName(context, ExchangeJobScheduler.class);
        JobInfo jobInfo;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            jobInfo = new JobInfo.Builder(32, componentName)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setMinimumLatency(Constants.JOB_SCHEDULER_INTERVAL)
                    .build();
        } else {
            jobInfo = new JobInfo.Builder(32, componentName)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPeriodic(Constants.JOB_SCHEDULER_INTERVAL)
                    .build();
        }
        return jobInfo;
    }


    @Provides
    @Singleton
    ExchangeDbHelper provideExchangeDbHelper(Context context) {
        return new ExchangeDbHelper(context);
    }

    @Provides
    @Singleton
    SqlBrite provideSqlBrite() {
        return new SqlBrite.Builder().build();
    }

    @Provides
    @Singleton
    BriteDatabase provideBriteDatabase(SqlBrite sqlBrite, ExchangeDbHelper exchangeDbHelper, RxSchedulersFactory rxSchedulersFactory) {
        return sqlBrite.wrapDatabaseHelper(exchangeDbHelper, rxSchedulersFactory.getIOScheduler());
    }


}
