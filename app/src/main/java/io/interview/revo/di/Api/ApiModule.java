package io.interview.revo.di.Api;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.interview.revo.BuildConfig;
import io.interview.revo.data.remote.RevoApi;
import io.interview.revo.data.remote.XMLConverterFactory;
import io.interview.revo.di.exchange.ExchangeScope;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by afeozzz on 13/02/17.
 */

@Module
public class ApiModule {
    @NonNull
    private final ChangeableBaseUrl changeableBaseUrl;

    public ApiModule(@NonNull String baseUrl) {
        changeableBaseUrl = new ChangeableBaseUrl(baseUrl);
    }

    @Provides
    @NonNull
    @Singleton
    public ChangeableBaseUrl provideChangeableBaseUrl() {
        return changeableBaseUrl;
    }

    @Provides
    @NonNull
    @Singleton
    OkHttpClient provideOkhttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY)).build();
    }

    @Provides
    @NonNull
    @Singleton
    RevoApi provideRevoRestApi(ChangeableBaseUrl changeableBaseUrl, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(XMLConverterFactory.create())
                .validateEagerly(BuildConfig.DEBUG)
                .baseUrl(changeableBaseUrl.url())
                .client(okHttpClient)
                .build()
                .create(RevoApi.class);
    }
}
