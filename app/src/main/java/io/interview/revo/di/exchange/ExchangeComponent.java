package io.interview.revo.di.exchange;

import android.app.job.JobScheduler;

import dagger.Component;
import dagger.Subcomponent;
import io.interview.revo.data.service.ExchangeJobScheduler;
import io.interview.revo.presentation.view.activity.MainActivity;

/**
 * Created by afeozzz on 13/02/17.
 */
@Subcomponent (modules = {ExchangeModule.class})
@ExchangeScope
public interface ExchangeComponent {
    void inject(MainActivity mainActivity);
    void inject(ExchangeJobScheduler jobScheduler);
}
