package io.interview.revo.di.application;

import javax.inject.Singleton;

import dagger.Component;
import io.interview.revo.data.service.ExchangeJobScheduler;
import io.interview.revo.di.Api.ApiModule;
import io.interview.revo.di.exchange.ExchangeComponent;
import io.interview.revo.di.exchange.ExchangeModule;

/**
 * Created by afeozzz on 13/02/17.
 */

@Component(modules = {AppModule.class, UtilsModule.class, ApiModule.class})
@Singleton
public interface AppComponent {
    ExchangeComponent plus(ExchangeModule exchangeModule);
}
