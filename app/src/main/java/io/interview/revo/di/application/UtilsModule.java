package io.interview.revo.di.application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.interview.revo.utils.RxSchedulers;
import io.interview.revo.utils.RxSchedulersFactory;

/**
 * Created by afeozzz on 13/02/17.
 */

@Module
public class UtilsModule {
    @Provides
    @Singleton
    RxSchedulersFactory provideRxSchedulersFactory() {
        return new RxSchedulers();
    }
}
