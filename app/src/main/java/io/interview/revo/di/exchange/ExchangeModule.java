package io.interview.revo.di.exchange;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.database.sqlite.SQLiteOpenHelper;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import dagger.Module;
import dagger.Provides;
import io.interview.revo.data.ExchangeRepository;
import io.interview.revo.data.local.ExchangeDbHelper;
import io.interview.revo.data.local.ExchangeLocalDataSource;
import io.interview.revo.data.remote.RevoApi;
import io.interview.revo.domain.ExchangeInteractor;
import io.interview.revo.presentation.presenter.ExchangePresenter;
import io.interview.revo.utils.RxSchedulersFactory;

/**
 * Created by afeozzz on 13/02/17.
 */

@Module
public class ExchangeModule {
    @Provides
    @ExchangeScope
    ExchangePresenter provideExchangePresenter(ExchangeInteractor exchangeInteractor,
                                               RxSchedulersFactory rxSchedulersFactory,
                                               JobScheduler jobScheduler,
                                               JobInfo jobInfo) {
        return new ExchangePresenter(exchangeInteractor, rxSchedulersFactory, jobScheduler, jobInfo);
    }


    @Provides
    @ExchangeScope
    ExchangeRepository provideExchangeRepository(ExchangeLocalDataSource exchangeLocalDataSource,
                                                 RevoApi revoApi) {
        return new ExchangeRepository(exchangeLocalDataSource, revoApi);
    }

    @Provides
    @ExchangeScope
    ExchangeInteractor provideExchangeInteractor(ExchangeRepository exchangeRepository, RxSchedulersFactory rxSchedulersFactory) {
        return new ExchangeInteractor(exchangeRepository, rxSchedulersFactory);
    }

    @Provides
    @ExchangeScope
    ExchangeLocalDataSource provideLocalDataSource(BriteDatabase briteDatabase) {
        return new ExchangeLocalDataSource(briteDatabase);
    }

}
