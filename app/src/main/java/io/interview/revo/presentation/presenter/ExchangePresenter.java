package io.interview.revo.presentation.presenter;


import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import io.interview.revo.data.local.CurrencyModel;
import io.interview.revo.data.remote.entities.CurrencyResponse;
import io.interview.revo.data.service.ExchangeJobScheduler;
import io.interview.revo.domain.ExchangeInteractor;
import io.interview.revo.presentation.ExchangeContract;
import io.interview.revo.utils.RxSchedulersFactory;
import rx.Observable;
import rx.Subscription;
import rx.exceptions.CompositeException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

import static io.interview.revo.data.remote.entities.CurrencyResponse.*;

/**
 * Created by afeozzz on 13/02/17.
 */

public class ExchangePresenter implements ExchangeContract.Presenter {
    private ExchangeContract.View view;

    private ExchangeInteractor exchangeInteractor;
    private RxSchedulersFactory rxSchedulersFactory;
    private JobScheduler jobScheduler;
    private JobInfo jobInfo;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public ExchangePresenter(ExchangeInteractor exchangeInteractor, RxSchedulersFactory rxSchedulersFactory,
                             JobScheduler jobScheduler, JobInfo jobInfo) {
        this.exchangeInteractor = exchangeInteractor;
        this.rxSchedulersFactory = rxSchedulersFactory;
        this.jobScheduler = jobScheduler;
        this.jobInfo = jobInfo;
    }

    @Override
    public void bindView(ExchangeContract.View exchangeView) {
        this.view = exchangeView;
    }

    @Override
    public void unbindView() {
        compositeSubscription.clear();
        this.view = null;
    }

    @Override
    public void runForegroundService() {
        jobScheduler.schedule(jobInfo);
        view.showProgress();
    }

    @Override
    public void stopForegroundService() {
        jobScheduler.cancelAll();
    }

    @Override
    public void handleChangesCurrencies() {
        Subscription subscription = exchangeInteractor.handleCurrencyChangesObserver()
                .compose(rxSchedulersFactory.<List<CurrencyModel>>applySchedulers())
                .subscribe(new Action1<List<CurrencyModel>>() {
                    @Override
                    public void call(List<CurrencyModel> revoCurrencies) {
                        view.showContent(revoCurrencies);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        view.showError(throwable);
                    }
                });

        compositeSubscription.add(subscription);
    }
}
