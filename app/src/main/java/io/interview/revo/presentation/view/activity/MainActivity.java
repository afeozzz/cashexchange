package io.interview.revo.presentation.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import io.interview.revo.App;
import io.interview.revo.R;
import io.interview.revo.data.local.CurrencyModel;
import io.interview.revo.di.exchange.ExchangeModule;
import io.interview.revo.presentation.ExchangeContract;
import io.interview.revo.presentation.presenter.ExchangePresenter;
import io.interview.revo.utils.Constants;

public class MainActivity extends AppCompatActivity implements ExchangeContract.View {

    @Inject
    ExchangePresenter exchangePresenter;

    private TextView euroTextView;
    private TextView dollarTextView;
    private TextView gbrTextView;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        App.get(this).applicationComponent().plus(new ExchangeModule()).inject(this);

        initUI();

        exchangePresenter.bindView(this);
        exchangePresenter.handleChangesCurrencies();
        exchangePresenter.runForegroundService();
    }

    private void initUI() {
        euroTextView = (TextView) findViewById(R.id.euro);
        dollarTextView = (TextView) findViewById(R.id.dollar);
        gbrTextView = (TextView) findViewById(R.id.gbr);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    @Override
    protected void onDestroy() {
        exchangePresenter.stopForegroundService();
        exchangePresenter.unbindView();
        super.onDestroy();
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showContent(List<CurrencyModel> currencyModelList) {
        progressBar.setVisibility(View.GONE);
        for (int i = 0, size = currencyModelList.size(); i < size; i++) {
            CurrencyModel currencyModel = currencyModelList.get(i);
            StringBuilder stringBuilder = new StringBuilder();
            if (currencyModel.getId().equalsIgnoreCase(Constants.DOLLAR_ID)) {
                stringBuilder.append("USD - ").append(currencyModel.getRate());
                dollarTextView.setText(stringBuilder.toString());
            }else if (currencyModel.getId().equalsIgnoreCase(Constants.EURO_ID)) {
                stringBuilder.append("EURO - ").append(currencyModel.getRate());
                euroTextView.setText(stringBuilder.toString());
            }else if (currencyModel.getId().equalsIgnoreCase(Constants.GBR_ID)){
                stringBuilder.append("GBR - ").append(currencyModel.getRate());
                gbrTextView.setText(stringBuilder.toString());
            }
        }
    }
}
