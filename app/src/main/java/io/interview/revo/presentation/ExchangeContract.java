package io.interview.revo.presentation;

import java.util.List;

import io.interview.revo.data.local.CurrencyModel;

/**
 * Created by afeozzz on 13/02/17.
 */

public class ExchangeContract {
    public interface Presenter {
        void bindView(View exchangeView);

        void unbindView();

        void runForegroundService();

        void stopForegroundService();

        void handleChangesCurrencies();
    }


    public interface View {
        void showError(Throwable throwable);

        void showProgress();

        void showContent(List<CurrencyModel> currencyModelList);
    }
}
