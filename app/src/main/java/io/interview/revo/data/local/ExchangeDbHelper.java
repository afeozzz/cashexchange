package io.interview.revo.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Singleton;

/**
 * Created by afeozzz on 13/02/17.
 */

public class ExchangeDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "Exchange.db";

    private static final String TEXT_TYPE = " TEXT";

    private static final String FLOAT_TYPE = " FLOAT";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ExchangePersistenceContract.ExchangeEntry.TABLE_NAME + " (" +
                    ExchangePersistenceContract.ExchangeEntry._ID + TEXT_TYPE + " PRIMARY KEY," +
                    ExchangePersistenceContract.ExchangeEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + " UNIQUE" + COMMA_SEP +
                    ExchangePersistenceContract.ExchangeEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    ExchangePersistenceContract.ExchangeEntry.COLUMN_RATE + FLOAT_TYPE +
                    " )";

    public ExchangeDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
