package io.interview.revo.data.local;

/**
 * Created by afeozzz on 13/02/17.
 */

public class CurrencyModel {
    private final String id;
    private final String title;
    private final float rate;

    public CurrencyModel(String id, String title, float rate) {
        this.id = id;
        this.title = title;
        this.rate = rate;
    }

    public float getRate() {
        return rate;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }
}
