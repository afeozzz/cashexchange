package io.interview.revo.data;

import java.util.List;

import io.interview.revo.data.local.CurrencyModel;
import io.interview.revo.data.remote.entities.CurrencyResponse;
import rx.Observable;

/**
 * Created by afeozzz on 13/02/17.
 */

public interface ExchangeDataSource {
    Observable<CurrencyModel> getCurrencyById(String id);

    Observable<List<CurrencyModel>> getCachedCurrencies();

    Observable<List<CurrencyModel>> getNewCurrencies();

    void saveCurrency(CurrencyModel currencyModel);
}
