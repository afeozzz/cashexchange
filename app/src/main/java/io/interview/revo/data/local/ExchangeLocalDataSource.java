package io.interview.revo.data.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.squareup.sqlbrite.BriteDatabase;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Provides;
import io.interview.revo.data.ExchangeDataSource;
import io.interview.revo.data.remote.entities.CurrencyResponse;
import rx.Observable;
import rx.functions.Func1;

import static io.interview.revo.data.local.ExchangePersistenceContract.*;
import static io.interview.revo.data.local.ExchangePersistenceContract.ExchangeEntry.*;
import static io.interview.revo.data.local.ExchangePersistenceContract.ExchangeEntry.COLUMN_NAME_ENTRY_ID;
import static io.interview.revo.data.local.ExchangePersistenceContract.ExchangeEntry.COLUMN_NAME_TITLE;
import static io.interview.revo.data.local.ExchangePersistenceContract.ExchangeEntry.COLUMN_RATE;

/**
 * Created by afeozzz on 13/02/17.
 */

@Singleton
public class ExchangeLocalDataSource implements ExchangeDataSource {

    BriteDatabase mDatabaseHelper;

    Func1<Cursor, CurrencyModel> mTaskMapperFunction;


    public ExchangeLocalDataSource(BriteDatabase briteDatabase) {
        mTaskMapperFunction = new Func1<Cursor, CurrencyModel>() {
            @Override
            public CurrencyModel call(Cursor cursor) {
                return getCurrency(cursor);
            }
        };
        this.mDatabaseHelper = briteDatabase;
    }

    @NonNull
    private CurrencyModel getCurrency(@NonNull Cursor c) {
        String itemId = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_ENTRY_ID));
        String title = c.getString(c.getColumnIndexOrThrow(COLUMN_NAME_TITLE));
        float rate =
                c.getFloat(c.getColumnIndexOrThrow(COLUMN_RATE));
        return new CurrencyModel(itemId, title, rate);
    }

    @Override
    public Observable<CurrencyModel> getCurrencyById(String id) {
        String[] projection = {
                COLUMN_NAME_ENTRY_ID,
                COLUMN_NAME_TITLE,
                COLUMN_RATE,
        };
        String sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?",
                TextUtils.join(",", projection), TABLE_NAME, COLUMN_NAME_ENTRY_ID);
        return mDatabaseHelper.createQuery(TABLE_NAME, sql, id)
                .mapToOneOrDefault(mTaskMapperFunction, null);
    }

    @Override
    public Observable<List<CurrencyModel>> getCachedCurrencies() {
        String[] projection = {
                COLUMN_NAME_ENTRY_ID,
                COLUMN_NAME_TITLE,
                COLUMN_RATE};
        String sql = String.format("SELECT %s FROM %s", TextUtils.join(",", projection), TABLE_NAME);
        return mDatabaseHelper.createQuery(TABLE_NAME, sql)
                .mapToList(mTaskMapperFunction);
    }

    @Override
    public Observable<List<CurrencyModel>> getNewCurrencies() {
        return null;
    }


    @Override
    public void saveCurrency(CurrencyModel currencyModel) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_ENTRY_ID, currencyModel.getId());
        values.put(COLUMN_NAME_TITLE, currencyModel.getTitle());
        values.put(COLUMN_RATE, currencyModel.getRate());
        mDatabaseHelper.insert(TABLE_NAME, values, SQLiteDatabase.CONFLICT_REPLACE);
    }
}
