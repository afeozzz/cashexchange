package io.interview.revo.data.remote.entities;

import android.util.SparseArray;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by afeozzz on 13/02/17.
 */

@Root(name = "gesmes", strict = false)
public class CurrencyResponse {

    @Element(type = Cube.class)
    public Cube cube;


    public static class Cube {
        @Element(type = InnerCube.class)
        public InnerCube innerCube;
    }

    public static class InnerCube {
        @Attribute(name = "time", required = false)
        public String time;

        @ElementList(inline = true, type = RevoCurrency.class)
        public List<RevoCurrency> revoCurrencies;

    }

    public static class RevoCurrency {

        @Attribute(name = "currency", required = false)
        public String currency;

        @Attribute(name = "rate", required = false)
        public Float rate;

    }
}
