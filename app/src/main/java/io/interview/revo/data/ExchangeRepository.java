package io.interview.revo.data;

import java.util.List;

import io.interview.revo.data.local.CurrencyModel;
import io.interview.revo.data.local.ExchangeLocalDataSource;
import io.interview.revo.data.remote.RevoApi;
import io.interview.revo.data.remote.entities.CurrencyResponse;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by afeozzz on 13/02/17.
 */

public class ExchangeRepository implements ExchangeDataSource {

    private final ExchangeLocalDataSource exchangeLocalDataSource;
    private final RevoApi revoApi;

    public ExchangeRepository(ExchangeLocalDataSource exchangeLocalDataSource, RevoApi revoApi) {
        this.exchangeLocalDataSource = exchangeLocalDataSource;
        this.revoApi = revoApi;
    }

    @Override
    public Observable<CurrencyModel> getCurrencyById(String id) {
        return exchangeLocalDataSource.getCurrencyById(id);
    }

    @Override
    public Observable<List<CurrencyModel>> getCachedCurrencies() {
        return exchangeLocalDataSource.getCachedCurrencies();
    }

    @Override
    public Observable<List<CurrencyModel>> getNewCurrencies() {
        return revoApi.getUpdatedCurrencies();
    }

    @Override
    public void saveCurrency(CurrencyModel currencyModel) {
        exchangeLocalDataSource.saveCurrency(currencyModel);
    }
}