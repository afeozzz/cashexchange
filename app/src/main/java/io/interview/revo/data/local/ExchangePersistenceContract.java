package io.interview.revo.data.local;

import android.provider.BaseColumns;

/**
 * Created by afeozzz on 13/02/17.
 */

public class ExchangePersistenceContract {
    private ExchangePersistenceContract() {}

    public static abstract class ExchangeEntry implements BaseColumns {
        public static final String TABLE_NAME = "currency";
        public static final String COLUMN_NAME_ENTRY_ID = "currency_id";
        public static final String COLUMN_NAME_TITLE = "currency_title";
        public static final String COLUMN_RATE = "currency_rate";
    }
}
