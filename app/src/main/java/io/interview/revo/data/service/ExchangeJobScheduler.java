package io.interview.revo.data.service;

import android.app.job.JobParameters;
import android.app.job.JobService;

import java.util.List;

import javax.inject.Inject;

import io.interview.revo.App;
import io.interview.revo.data.local.CurrencyModel;
import io.interview.revo.di.exchange.ExchangeModule;
import io.interview.revo.domain.ExchangeInteractor;
import io.interview.revo.utils.RxSchedulers;
import io.interview.revo.utils.RxSchedulersFactory;
import rx.Subscription;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by afeozzz on 14/02/17.
 */

public class ExchangeJobScheduler extends JobService {

    @Inject
    ExchangeInteractor exchangeInteractor;

    @Inject
    RxSchedulersFactory rxSchedulersFactory;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();


    @Override
    public void onCreate() {
        super.onCreate();
        App.get(this).applicationComponent().plus(new ExchangeModule()).inject(this);
    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        Subscription subscription = exchangeInteractor.getNewCurrencies()
                .subscribe(new Action1<List<CurrencyModel>>() {
                    @Override
                    public void call(List<CurrencyModel> currencyModels) {
                        CurrencyModel currencyModel = new CurrencyModel("EURO", "EURO", 1f);
                        exchangeInteractor.saveCurrency(currencyModel);
                        jobFinished(params, true);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        jobFinished(params, true);


                    }
                });
        compositeSubscription.add(subscription);
        return true;
    }

    @Override
    public void onDestroy() {
        compositeSubscription.clear();
        super.onDestroy();
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }
}
