package io.interview.revo.data.remote;

import java.util.List;

import io.interview.revo.data.local.CurrencyModel;
import io.interview.revo.data.remote.entities.CurrencyResponse;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by afeozzz on 13/02/17.
 */

public interface RevoApi {

    @GET("stats/eurofxref/eurofxref-daily.xml")
    Observable<List<CurrencyModel>> getUpdatedCurrencies();
}
