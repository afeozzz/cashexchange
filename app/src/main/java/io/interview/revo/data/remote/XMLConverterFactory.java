package io.interview.revo.data.remote;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.interview.revo.data.local.CurrencyModel;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by afeozzz on 14/02/17.
 */

public class XMLConverterFactory extends Converter.Factory {
    public static XMLConverterFactory create() {
        return new XMLConverterFactory();
    }

    @Override
    public Converter<ResponseBody, ArrayList<CurrencyModel>> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        return new Converter<ResponseBody, ArrayList<CurrencyModel>>() {
            @Override
            public ArrayList<CurrencyModel> convert(ResponseBody value) throws IOException {
                return parse(value.string());
            }
        };
    }

    public static ArrayList<CurrencyModel> parse(String xml) {
        ArrayList<CurrencyModel> currencyModels = new ArrayList<>();
        XmlPullParser ecbParser;
        try {
            ecbParser = XmlPullParserFactory.newInstance().newPullParser();
            ecbParser.setInput(new StringReader(xml));
            int eventType = ecbParser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (ecbParser.getName().equals("Cube") && ecbParser.getAttributeCount() == 2) {
                        currencyModels.add(new CurrencyModel(ecbParser.getAttributeValue(0),
                                ecbParser.getAttributeValue(0), Float.parseFloat(ecbParser.getAttributeValue(1))));
                    }
                }
                eventType = ecbParser.next();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return currencyModels;
    }
}
