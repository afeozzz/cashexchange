package io.interview.revo;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import io.interview.revo.di.Api.ApiModule;
import io.interview.revo.di.application.AppComponent;
import io.interview.revo.di.application.AppModule;
import io.interview.revo.di.application.DaggerAppComponent;

/**
 * Created by afeozzz on 13/02/17.
 */

public class App extends Application {
    @NonNull
    private AppComponent appComponent;

    @NonNull
    public static App get(@NonNull Context context) {
        return (App) context.getApplicationContext();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildComponents();
    }

    private AppComponent buildComponents() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule("http://www.ecb.europa.eu/"))
                .build();
    }

    @NonNull
    public AppComponent applicationComponent() {
        return appComponent;
    }
}
